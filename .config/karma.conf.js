module.exports = function(config) {
  config.set({
     // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '../',
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine', 'requirejs'],
    files: [
	    {pattern: 'robots.txt', included: false},
	    {pattern: 'node_modules/d3/dist/d3.js', included: false},
	    {pattern: 'node_modules/js-yaml/dist/js-yaml.js', included: false},
	    {pattern: 'node_modules/mathjs/dist/math.js', included: false},
	    {pattern: 'node_modules/marked/lib/marked.js', included: false},
	    //{pattern: 'node_modules/**/*.js', included: false},
			{pattern: 'staticApp/**/*.yml', included: false},
	    {pattern: 'staticApp/**/*.js', included: false},
	    '.config/test-main.js'
    ],
    // exclude: [
	 //    'node_modules/**/*.(test|spec|mock).js',
	 //    'node_modules/**/test/*.js',
	 //    'staticApp/main.js'
    // ],
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor

	  //FIXME: reactiver le coverage quand karma-coverage supportera les async/await
    // preprocessors: {
	 //    'staticApp/**/!(*.test|*.spec|*.mock).js': 'coverage'
    // },

    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress','coverage','longest','coveralls'], // ['progress','coverage','coveralls'] // min
	  longestSpecsToReport: 3,
	  coverageReporter: {
		  reporters:[
			  {type: 'lcov', dir:'coverage/'},
			  {type: 'text-summary', dir:'coverage/', file:'summary.txt'}
		  ],
		  includeAllSources: true,
		  check: {
			  global: { // thresholds for all files
				  statements: 10,
				  lines: 10,
				  branches: 10,
				  functions: 10
			  },
			  each: { // thresholds per file
				  statements: 0,
				  lines: 0,
				  branches: 0,
				  functions: 0
			  }
		  },
		  watermarks: {
			  statements: [ 50, 90 ],
			  functions: [ 50, 90 ],
			  branches: [ 50, 90 ],
			  lines: [ 50, 90 ]
		  }
		},
	  port: 9876,
    colors: true,
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,
    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
	  //browsers: ['Chrome', 'Firefox', 'PhantomJS'],
	  browsers: [],
	  autoWatch: true,
    singleRun: false,
    concurrency: Infinity,
	  customLaunchers: {
		  Chrome_travis_ci: {
			  base: 'Chrome',
			  flags: ['--no-sandbox']
		  },
		  ChromeNoSandbox: {
			  base: 'Chrome',
			  flags: ['--no-sandbox']
		  }
	  }
  })
};
