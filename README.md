[![build status](https://framagit.org/mycelia/mycelia-front-app/badges/master/build.svg)](https://framagit.org/mycelia/mycelia-front-app/pipelines)
[![coverage report](https://framagit.org/mycelia/mycelia-front-app/badges/master/coverage.svg)](https://mycelia.tools/ci/mycelia-front-app/coverage/)
[![maintainability](https://mycelia.tools/ci/mycelia-front-app/plato/maintainability.svg)](https://mycelia.tools/ci/mycelia-front-app/plato/)
[![License: EUPL](https://demo.mycelia.tools/licence-EUPL.svg)](LICENSE.md) compatible 
[![License: AGPL v3](https://demo.mycelia.tools/licence-AGPLv3.svg)](http://www.gnu.org/licenses/agpl-3.0)

[![release](https://img.shields.io/npm/v/mycelia-front-app.svg)](https://www.npmjs.com/package/mycelia-front-app)
[![usage as download](https://img.shields.io/npm/dy/mycelia-front-app.svg)](https://www.npmjs.com/package/mycelia-front-app)



# Mycelia
Visualisation de graph sociaux

## [Démo en ligne](https://mycelia.frama.io/mycelia-front-app/)

## Usage basique (sans authentification ni édition des données)

- [Télécharger le projet pré-optimisé](https://framagit.org/mycelia/mycelia-front-app/-/jobs/artifacts/master/download?job=staticPackage)
- Remplacer le contenu du dossier `allData` par vos données et configuration (éditable manuellement par vos soins)
- Héberger le résultat ou vous voulez (ou ouvrir directement `index.html` dans votre navigateur pour consulter en local, même **hors ligne**)

## Usage avancé

Voir : [mycelia-cli](https://framagit.org/mycelia/mycelia-cli) et [mycelia-server-nodejs](https://framagit.org/mycelia/mycelia-server-nodejs)

## Contribuer

- Au code
- Aux traductions
- Aux remontées de bugs 
- Aux propositions d'évolutions
- Au financement [![leberapay](https://img.shields.io/liberapay/receives/1000i100.svg)](https://liberapay.com/1000i100/)
- A la promotion

Voir : [CONTRIBUTING.md](CONTRIBUTING.md)

## Evolution du logiciel
- [CHANGELOG.md](CHANGELOG.md)
- [Issues](https://framagit.org/mycelia/mycelia-front-app/issues)
- [Roadmap : objectifs et priorités d'évolution](https://framagit.org/mycelia/mycelia-front-app/wikis/roadmap)
<!-- https://framagit.org/mycelia/mycelia-front-app/milestones -->
