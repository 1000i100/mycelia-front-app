/*
 servir la cache + demander le commit-hash (s'il na pas été demandé il y a moins d'une minute).
 Si différent de celui en cache, mettre à jour en arrière plan tout le contenu de la cache.

 différencier :
 - la version de mycelia-front-app -> mettre à jour tout sauf allData/
 - le commit-hash branche project -> mettre à jour allData/ sauf data.yml (ou ce qui en découlera et trad à déterminer)
 - le commit-hash branch data -> mettre à jour data.yml (ou ce qui en découlera et trad à déterminer)

 */
let MIN_UPDATE_DELAY = 5; // secondes
const UNCACHABLE_DELAY = 60; // secondes
const LOCALHOST_DELAY = 1; // secondes

self.addEventListener('install', function (event) {
	console.log('SW: service worker installing it-self');
	skipWaiting();
});
self.addEventListener('activate', function (event) {
	console.log('SW: service worker activating');
});
self.addEventListener('fetch', event => {
	const url = event.request.url+'';
	event.respondWith(async function () {
		const cacheRes = await caches.match(url);
		if (cacheRes) {
			updateCache();
			return cacheRes;
		} else {
			return await network2cache(url);
		}
	}());
});
async function network2cache(url){
	const res = await fetch(url, {cache: "no-store"});
	const store = await caches.open('mycelia');
	await store.put(url, res);
	return caches.match(url);
}
let updating = false;
let oldCache;
async function updateCache(){
	if(updating) return ;
	updating = true;
	const obsolete = {};
	if(!oldCache){
		const cacheRep = await caches.match("cacheVersion");
		oldCache = cacheRep?(await cacheRep.json()): {};
	}

	if(oldCache.time > Date.now()- MIN_UPDATE_DELAY *1000) return updating = false;// && console.log(`SW: last update too close ${Math.round((Date.now()/10 - MIN_UPDATE_DELAY*100) - oldCache.time/10)/100}s < ${MIN_UPDATE_DELAY}s`);
	if(registration.scope.indexOf('localhost') !== -1){
		// Localhost mode :
		obsolete.data = true;
		obsolete.project = true;
		obsolete.app = true;
		MIN_UPDATE_DELAY = LOCALHOST_DELAY;
		oldCache = noCache();
	}
	if(!oldCache.time || oldCache.data){
		const response = await fetch("./allData/cache.json", {cache: "no-store"});
		if(response.status === 200){
			const json = await response.json();
			if(oldCache.data !== json.commitHash) obsolete.data = true;
			if(oldCache.project !== json.commitHash) obsolete.project = true;
			if(oldCache.app !== json.appVersion) obsolete.app = true;
			oldCache = {
				time:Date.now(),
				data:json.commitHash,
				project:json.commitHash,
				app:json.appVersion
			};
			const store = await caches.open('mycelia');
			store.put("cacheVersion", new Response(JSON.stringify(oldCache)));
		}
	}
	if(!oldCache.data && !obsolete.data) {
		// no cache.json file and not in localhost.
		obsolete.data = true;
		obsolete.project = true;
		obsolete.app = true;
		MIN_UPDATE_DELAY = UNCACHABLE_DELAY;
		oldCache = noCache();
	}

	if(obsolete.data || obsolete.project || obsolete.app){
		console.log("SW: background updating : ",obsolete);
		const store = await caches.open('mycelia');
		const urls = await store.keys();
		if(obsolete.data) urls.filter(req=>req.url.indexOf('data.yml')!==-1).forEach(network2cache);
		if(obsolete.project) urls.filter(req=>(req.url.indexOf('allData/')!==-1)&&(req.url.indexOf('data.yml')===-1)).forEach(network2cache);
		if(obsolete.app) urls.filter(req=>(req.url.indexOf('allData/')===-1)&&(req.url.indexOf('cacheVersion')===-1)).forEach(network2cache);
	}
	updating = false;
}
function noCache(){
	const oldCache = {
		time:Date.now(),
		data:false,
		project:false,
		app:false
	};
	caches.open('mycelia').then(store=>store.put("cacheVersion", new Response(JSON.stringify(oldCache))));
	return oldCache;
}
