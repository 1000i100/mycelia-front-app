define([
	'd3',
	'./smartEvents',
	'./trad/trad',
	'./configLoader'
], (d3, ev, trad, cfg) => {
	'use strict';
	const on = ev.on, send = ev.send, t = trad.t;

	function init() {
		listenerInit();
	}

	function listenerInit() {
		//on('config.ready', initUI);
		on('hashchange', render);
		on('render', updatePanels);
		ev.need('config', render);
		on('err', displayErr);
		on('rendered', ()=>{ // noticeView, formView, tradView
			document.querySelector('#details>.expand').addEventListener('click',expandPanel);
			document.querySelector('#details>.reduce').addEventListener('click',reducePanel);
		});
		ev.whenInDom('#details>.expand',()=>{
			document.querySelector('#details>.expand').addEventListener('click',expandPanel);
			document.querySelector('#details>.reduce').addEventListener('click',reducePanel);
		});
	}
	function displayErr(err){
		if(!err.type) return console.error("err:",err);
		let errBox = getOrCreate('errBox');
		let errLine = document.createElement('a');
		errLine.innerHTML = buildErrMessage(err);
		errLine.setAttribute('class','err');
		errLine.setAttribute('data-timestamp',Date.now());
		errLine.setAttribute('href','#{"tutorial":"err.'+err.type+'"}');
		errBox.appendChild(errLine);
		setTimeout(fadeErr,10000);
		setTimeout(fadeErr,15000);
	}
	function fadeErr(){
		const errList = document.querySelectorAll('#errBox .err');
		for(let err of errList){
			if(err.getAttribute('data-timestamp')<Date.now()-10000 && !err.classList.contains('fade'))
				err.classList.add('fade');
			if(err.getAttribute('data-timestamp')<Date.now()-15000)
				err.parentNode.removeChild(err);
		}
	}
	const errMessageBuilderCatalog = {};
	errMessageBuilderCatalog["file.load.err.404"] = (err)=>{
		if(err.value.indexOf('allData') !== -1){
			return t('err 404, file not found : ')+'allData'+err.value.split('allData')[1];
		}
		return t('err 404, file not found : ')+err.value;
	};
	errMessageBuilderCatalog["default"] = (err) => 'err : '+JSON.stringify(err);
	function buildErrMessage(err){
		console.log(err.type);
		if(errMessageBuilderCatalog[err.type]) return errMessageBuilderCatalog[err.type](err);
		else return errMessageBuilderCatalog['default'](err);
	}
	function getOrCreate(id){
		let node = document.getElementById(id);
		if(!node){
			node = document.createElement('div');
			node.setAttribute('id',id);
			document.body.appendChild(node);
		}
		return node;
	}
	function render() {
		send('render', cfg.getConfig());
	}

	d3.selectAll('.expand').on('click', expandPanel);
	d3.selectAll('.reduce').on('click', reducePanel);

	function expandPanel(e) {
		slidePanel(e,{'hidden': 'small', 'small': 'big'});
	}

	function reducePanel(e) {
		slidePanel(e,{'big': 'small', 'small': 'hidden'});
	}

	function slidePanel(e, nextStatePicker) {
		// met à jour l'état de l'app en modifiant la config
		let panelNode = d3.event?d3.event.target.parentNode:e.target.parentNode;
		cfg.getConfig().panels[panelNode.id] = nextStatePicker[panelNode.className];
	}


	function updatePanels(config) {
		let panelsMoved = false;
		for (let i in config.panels) {
			let node = d3.select('#' + i);
			let alreadyClassed = node.classed(config.panels[i]);
			if (!alreadyClassed) {
				node.node().className = config.panels[i];
				panelsMoved = true;
			}
		}
		if (panelsMoved) send('resize');
	}
	return {init};
});
