define([
	'd3',
	'./smartEvents',
	'./fps',
	'./trad/trad',
	'./formLoader',
	'./graphDataLoader',
	'./structManipulation',
	'./json2dom',
	'mathjs'
], (d3, ev, fps, trad, formLoader, graphDataLoader, struct, json2dom, math) => {
	'use strict';
	window.math = math;
	const on = ev.on, send = ev.send, t = trad.t, multiTimeout = fps.multiTimeout;
	let options, filteredGraph, displayedGraph = {};
	ev.need("config", (conf) => {
		options = conf;
		ev.send("graph.config.ok");
	});

	on('resize', () => multiTimeout(50, 500, ()=>send('updateSvgArea') ));
	ev.need('filteredGraph', () => multiTimeout(50, 500, ()=>send('updateSvgArea') ));

	on('fullGraph change', filterGraph);
	on("filteredGraph.filterTime", (filteringGraph) => {
		filteredGraph = filteringGraph;
		ev.give('filteredGraph',filteredGraph);
	}, 1);
	ev.need("fullGraph", filterGraph);
	function filterGraph(graph){
		const filteringGraph = struct.clone(graph);
		if (!filteringGraph.node) filteringGraph.node = [];
		if (!filteringGraph.link) filteringGraph.link = [];
		// clean dead-end links
		filteringGraph.link = filteringGraph.link.filter(l=>{
			if(filteringGraph.node.findIndex( (n)=>n.id === l.source ) !== -1
				&& filteringGraph.node.findIndex( (n)=>n.id === l.target ) !== -1){
				return true;
			} else{
				console.error(`Err: link between missing node(s) : ${l.source} --- ${l.id} --> ${l.target}`);
				return false;
			}
		});
		send("filteredGraph.filterTime", filteringGraph);
		ev.send("graph.filteredGraph.ok");

	}

	ev.after(['graph.config.ok', 'graph.filteredGraph.ok', 'form.template.ready', 'trad loaded'],
		() => setTimeout(() => send('graph.init'), 10) );

	ev.after('graph.init', function () {



		var zoom = d3.zoom()
			.scaleExtent([options.zoomMin, options.zoomMax])
			.on("zoom", zoomed);

		document.querySelector("#viz").innerHTML = ""; // on vire l'anim de chargement
		var svg = d3.select("#viz").call(zoom).on("dblclick.zoom", null),
			width = window.innerWidth,
			height = window.innerHeight;
		svg.classed("loading", false);
		if(options.LoD > 5) svg.classed("LoD_hover", true);
		if(options.LoD > 7) svg.classed("LoD_alwaysArrow", true);
		if(options.LoD > 9) svg.classed("LoD_transition", true);



		// build the arrow.
		svg.append("defs").selectAll("marker")
			.data(["end"])      // Different link/path types can be defined here
			.enter().append("marker")    // This section adds in the arrows
			.attr("id", String)
			.attr("viewBox", "-3 -7 13 14")
			.attr("refX", 7.5)
			.attr("refY", 0)
			.attr("markerWidth", 5)
			.attr("markerHeight", 5)
			.attr("orient", "auto")
			.append("path")
			.attr("id", "fleche")
			.attr("d", "M0,0 L -2,5 10,0 -2,-5 0,0 Z"); // flèche droite
		//.attr("d", "M0,0 C 0,1 -5,6 0,5 5,4 10,1 10,0 10,-1 5,-4 0,-5 -5,-6 0,-1 0,0 Z"); // flèche arrondi
		//.attr("d", "M0,-5L10,0L0,5");

		var zoomableContainer = svg.append("g")
			.attr("class", "zoomableContainer");
		updateZoom();
		var centre = zoomableContainer.append("g")
			.attr("class", "centre")
			.attr("transform", 'translate(' + width / 2 + ',' + height / 2 + ')');

		centre.append("image")
			.attr("xlink:href", options.centerLogoUrl)
			.attr("x", -options.centerLogoWidth / 2)
			.attr("y", -options.centerLogoHeight / 2)
			.attr("width", options.centerLogoWidth)
			.attr("height", options.centerLogoHeight)
			.attr("opacity", options.centerLogoOpacity)
		;
		var color = d3.scaleOrdinal(d3.interpolateSpectral);

		var simulation = d3.forceSimulation()
			.alphaDecay(options.friction)
			.force("noCollision", d3.forceManyBody().distanceMax(2 * options.nodeBaseRadius * options.nodeMinRatio).strength(options.noCollisionForce))
			.force("proximityWarning", d3.forceManyBody().distanceMax(options.proximityWarningDistanceRatio * 2 * options.nodeBaseRadius * options.nodeMaxRatio).strength(options.proximityWarningForce))
			.force("gazDispersion", d3.forceManyBody().distanceMax(options.gazDispersionMaxDistanceRatio * 2 * options.nodeBaseRadius * options.nodeMaxRatio).strength(options.gazDispersionForce))
		;
		on('updateSvgArea',updateSvgArea);
		send('updateSvgArea');
		function updateSvgArea() {
			var legendWidth = document.querySelector('#legend').offsetLeft + document.querySelector("#legend").offsetWidth,
				detailsWidth = window.innerWidth - document.querySelector("#details").offsetLeft,
				toolsHeight = document.querySelector('#tools-panel').offsetTop + document.querySelector("#tools-panel").offsetHeight;
			width = window.innerWidth - legendWidth - detailsWidth;
			height = window.innerHeight - toolsHeight;
			svg.attr("width", width).attr("height", height).attr("style", 'left:' + legendWidth + 'px;top:' + toolsHeight + 'px');
			centre.attr("transform", 'translate(' + width / 2 + ',' + height / 2 + ')');
			updateZoom();
			applyFixedNode();
			updateSelection();
			agitationTemporaire(options.boostAgitation.temps, options.boostAgitation.force);
		}
		simulation.on("tick", ticked);
		updateCenterForce();

		let allLinksG = zoomableContainer.append("g")
			.attr("class", "links");
		let allNodesG = zoomableContainer.append("g")
			.attr("class", "nodes");

		let link = allLinksG
			.selectAll("g.link");
		let node = allNodesG
			.selectAll("g.node");


		function applyOn(onThisJson, overwritingJson) {
			for (let key in overwritingJson) {
				if (typeof overwritingJson[key] === "object" && typeof onThisJson[key] !== 'undefined') {
					onThisJson[key] = applyOn(onThisJson[key], overwritingJson[key]);
				}
				else onThisJson[key] = struct.clone(overwritingJson[key]);
			}
			return onThisJson;
		}
		function updateDisplayedGraphWithFilteredGraph(filteredGraph, displayedGraph) {
			if (!displayedGraph.node) displayedGraph.node = [];
			if (!displayedGraph.link) displayedGraph.link = [];
			filteredGraph.node.forEach((filteredItem)=>{
				const dispItem = displayedGraph.node.find( (dispItem)=>filteredItem.id === dispItem.id );
				if(dispItem) applyOn(dispItem,filteredItem);
				else displayedGraph.node.push(struct.clone(filteredItem));
			});
			filteredGraph.link.forEach((filteredItem)=>{
				const dispItem = displayedGraph.link.find( (dispItem)=>filteredItem.id === dispItem.id );
				if(dispItem) applyOn(dispItem,filteredItem);
				else displayedGraph.link.push(struct.clone(filteredItem));
			});
			displayedGraph.node = displayedGraph.node.filter( (item)=> filteredGraph.node.findIndex( (refItem)=>refItem.id === item.id ) !== -1 );
			displayedGraph.link = displayedGraph.link.filter( (item)=> filteredGraph.link.findIndex( (refItem)=>refItem.id === item.id ) !== -1 );

		}
		ev.need('filteredGraph',updateGraph);
		on('filteredGraph.ready',updateGraph);
		function updateGraph(graph) {
			updateDisplayedGraphWithFilteredGraph(graph, displayedGraph);
			simulation
				.nodes(displayedGraph.node)
				.force("link",
					d3.forceLink()
						.id(function (d) {
							return d.id;
						})
						.links(displayedGraph.link)
				)
			;
			computeNodesDegree(displayedGraph);

			let nodeRadiusScale = d3.scaleLinear()
				.domain(d3.extent(displayedGraph.node, function (n) {
					return n.degree;
				}))
				.range([options.nodeMinRatio, options.nodeMaxRatio]);

			link = link.data(displayedGraph.link, d => d.id );
			node = node.data(displayedGraph.node, d => d.id );

			/*
			 let link = allLinksG
			 .selectAll("line")
			 .data(displayedGraph.link);
			 */

			let linkEnter = link.enter()
				.append("g")
				.attr("class", "link")
				.attr("id", (n) => n.id)
				.on("click", selectLink);
			linkEnter.append("line")
				.attr("stroke-width", (d) => {
					d.calculatedValue = 2 * Math.sqrt(d.value || 1);
					return d.calculatedValue;
				});
			//.attr("marker-end", "url(#end)");
			if(options.LoD > 4)
				linkEnter.append("use")
					.attr("xlink:href", "#fleche");


			link.exit().remove();
			link = linkEnter.merge(link);//.attr("class","merged");

			/*				let node = allNodesG
			 .selectAll("g.node")
			 .data(displayedGraph.node);*/
			let nodeEnter = node.enter()
				.append("g")
				.attr("class", "node")
				.attr("id", function (n) {
					return n.id;
				})
				.on("mouseover", partialHideNotConcerned)
				.on("mouseout", unhideAll)
				.on("dblclick", dblclick)
				.call(d3.drag()
					.on("start", dragstarted)
					.on("drag", dragged)
					.on("end", dragended))
			;

			nodeEnter.append("circle")
				.attr("cx", 0).attr("cy", 0)
				.attr("r", function (n) {
					n.r = options.nodeBaseRadius * nodeRadiusScale(n.degree);
					return n.r * options.nodeStyle.selectionSize
				})
				//.attr("opacity", .2) //FIXME: nombre magique
				.attr("fill", function (d) {
					return color(0);//FIXME: couleur magique
				});
			nodeEnter.append("path")
				.attr("d", function (n) {
					return points2Path(regularPolygon(0, 0, n.r * options.nodeStyle.selectionSize, options.fixedNode.sides), options.fixedNode.tension) + ' z';
				})
				.attr("transform", "rotate(22.5)")
				.attr("fill", function (d) {
					return color(0);//FIXME: couleur magique
				});

			computeVoisins(displayedGraph);
			if(options.LoD > 1) {
				// image par calques.
				const domLayerList = json2dom.json2dom(options.layers);
				const flatFinalLayers = json2dom.xpath("//*[criterion]", domLayerList);
				const domGraph = json2dom.json2dom(filteredGraph);
				for (let domLayer of flatFinalLayers) {
					const layer = json2dom.dom2json(domLayer);
					if (options.hideLayers[layer.name]) continue;

					const selected = json2dom.xpath(layer.criterion, domGraph, XPathResult.UNORDERED_NODE_ITERATOR_TYPE);
					const selectedSet = {};
					for (let domNode of selected) {
						let id = domNode.querySelector('id').innerText;
						let type = id.split('-')[0];
						//if(type === "link") continue;
						selectedSet[id] = true;
					}
					nodeEnter
						.filter((n) => selectedSet[n.id])
						.classed(layer.name, true);
					linkEnter
						.filter((n) => selectedSet[n.id])
						.classed(layer.name, true);

					if (layer.name.search(RegExp(options.layersPicto2GraphRegExFilter)) === -1) continue; //FIXME: virrer cette restriction

					if (options.LoD > 8) {
						nodeEnter
							.filter((n) => selectedSet[n.id])
							.append("image")
							.attr("xlink:href", options.layersPictoFolder + layer.name + ".svg")
							.attr("x", function (n) {
								return -n.r / 2;
							})
							.attr("y", function (n) {
								return -n.r / 2;
							})
							.attr("width", function (n) {
								return n.r;
							})
							.attr("height", function (n) {
								return n.r;
							}).on("error", build_imageLoadErrorPlaceOverFunc(layer.name));
					}
				}
			}
			if(options.LoD > 3) {
				nodeEnter.append("text")
					.attr("text-anchor", "middle")
					.attr("dx", 0)
					.attr("dy", function (n) {
						return n.r;
					})
					.text(function (n) {
						return t(n.label)
					});
			}
			if(options.LoD > 1) {
				nodeEnter.append("title")
					.text(function (n) {
						return t(n.label);
					});
			}

			node.exit().remove();
			node = nodeEnter.merge(node);

			link.each((d) => {
				const g = d3.select('#' + d.id);
				g.classed('voisin_' + d.source.id, true);
				g.classed('voisin_' + d.target.id, true);
			});
			node.each((d) => {
				const g = d3.select('#' + d.id);
				for (let v of d.voisins) g.classed('voisin_' + v.id, true);
			});


			var linkLengthScale = d3.scaleLinear()
				.domain(d3.extent(displayedGraph.node, function (n) {
					return n.degree;
				}))
				.range([options.linkLengthMinRatio, options.linkLengthMaxRatio]);

			simulation.force("link")
				.strength(function strength(link) {
					return options.linkStrengthRatio / Math.min(link.source.degree, link.target.degree);
				})
				.distance(function (link) {
					var distanceMin = (link.source.r + link.target.r);
					return distanceMin + distanceMin * linkLengthScale(Math.min(link.source.degree, link.target.degree));
				})
			;
			//simulation.restart()
			agitationTemporaire(options.boostAgitation.temps, options.boostAgitation.force);
		}

		//updateGraph(filteredGraph);
		function ticked() {
			allNodesG.selectAll("g.node")
				.attr("transform", function (n) {
					if (options.confiner) {
						var zx = centerRatioX2D3X(options.zoom.x),
							zy = centerRatioY2D3Y(options.zoom.y);
						var limiteGauche = n.r - zx / options.zoom.k;
						var limiteHaut = n.r - zy / options.zoom.k;
						var limiteDroite = (width - zx) / options.zoom.k - n.r;
						var limiteBas = (height - zy) / options.zoom.k - n.r;
						n.x = Math.max(limiteGauche, Math.min(limiteDroite, n.x));
						n.y = Math.max(limiteHaut, Math.min(limiteBas, n.y));
					}
					if (isNaN(n.x)) n.x = 0;
					if (isNaN(n.y)) n.y = 0;
					if (isNaN(n.r)) n.r = 0;
					if (isNaN(n.vx)) n.vx = 0;
					if (isNaN(n.vy)) n.vy = 0;
					return "translate(" + n.x + "," + n.y + ")";
				});
			allLinksG.selectAll("g").each((d) => {
				// calcul vectoriel
				const arrowSize = Math.sqrt(d.calculatedValue / 2);
				const source = [d.source.x, d.source.y];
				const target = [d.target.x, d.target.y];
				const fullVector = math.subtract(target, source);
				const normalizedVector = math.divide(fullVector, math.norm(fullVector));
				//const lineOrigin = math.add(source, math.multiply(normalizedVector,d.source.r/2));
				const lineTarget = math.subtract(target, math.multiply(normalizedVector, d.target.r / 2 + arrowSize * 11));

				//let angleRad = math.acos(math.dot(normalizedVector, [1, 0]));
				//if (normalizedVector[1] < 0) angleRad = -angleRad;
				//const angleDeg = math.unit(angleRad, "rad").to("deg").toNumber();
				// stockage résultat
				d.x1 = source[0];
				d.y1 = source[1];
				d.x2 = lineTarget[0];
				d.y2 = lineTarget[1];
				//d.arrowTransform = "translate(" + d.x2 + "," + d.y2 + ") rotate(" + angleDeg + ") scale(" + arrowSize + ")";

				const scaledVector = math.multiply(normalizedVector, arrowSize);
				d.arrowTransform = `matrix(${scaledVector[0]},${scaledVector[1]},${-scaledVector[1]},${scaledVector[0]},${d.x2},${d.y2})`;
			});
			allLinksG.selectAll("line")
				.attr("x1", (d) => d.x1) // affichage
				.attr("y1", (d) => d.y1) // affichage
				.attr("x2", (d) => d.x2) // affichage
				.attr("y2", (d) => d.y2) // affichage
			;
			allLinksG.selectAll("use").attr("transform", (d) => d.arrowTransform); // affichage
			fps.tick();
		}


		function zoomed() {
			options.zoom = {
				"x": round(d3X2CenterRatioX(d3.event.transform.x, d3.event.transform.k), options.zoomPrecisionXY),
				"y": round(d3Y2CenterRatioY(d3.event.transform.y, d3.event.transform.k), options.zoomPrecisionXY),
				"k": round(d3.event.transform.k, options.zoomPrecisionK)
			};
			updateZoom();
			if (options.confiner) agitationTemporaire(options.boostAgitation.temps, options.boostAgitation.force);
		}

		function dragstarted(d) {
			if (!d3.event.active) simulation.alphaTarget(options.boostAgitation.force).restart();
			d.dragOriginX = d.x;
			d.dragOriginY = d.y;
			d.wasSelected = false;
			var id = getNearestAncestorId(d3.event.sourceEvent.target);
			if (options.selected !== id) options.selected = id;
			else d.wasSelected = true;
			//updateSelection();
			updateCenterForce();
		}

		function dragged(d) {

			if (Math.abs(d3.event.x - d.dragOriginX) > 5 || Math.abs(d3.event.y - d.dragOriginY) > 5) { //FIXME: magic number
				if (d.fx == null) {
					d3.select(this).classed("fixed", d.fixed = true);
				}
				d.fx = d3.event.x;
				d.fy = d3.event.y;
			}
		}

		function dragended(d) {
			if (Math.abs(d3.event.x - d.dragOriginX) > 5 || Math.abs(d3.event.y - d.dragOriginY) > 5) { //FIXME: magic number
				var d3ToCentricX = d3.scaleLinear().domain([0, width]).range([-1, 1]);
				var d3ToCentricY = d3.scaleLinear().domain([0, height]).range([-1, 1]);
				if (!options.fixedNodes) options.fixedNodes = {};
				var id = getNearestAncestorId(d3.event.sourceEvent.target);
				options.fixedNodes[id] = {
					"x": round(d3ToCentricX(d.x), 2),
					"y": round(d3ToCentricY(d.y), 2)
				};
			} else if (d.wasSelected) {
				options.selected = undefined;
			}
			//updateSelection();
			agitationTemporaire(options.boostAgitation.temps, options.boostAgitation.force);
		}

		function dblclick(d) {
			d3.select(this).classed("fixed", d.fixed = false);
			d.fx = null;
			d.fy = null;
			var id = d.id;
			options.fixedNodes[id] = undefined;
			updateZoom();
			updateCenterForce();
			agitationTemporaire(options.boostAgitation.temps, options.boostAgitation.force);
		}

		function selectLink(l) {
			if (options.selected !== l.id) options.selected = l.id;
			else options.selected = "";
		}


		function applyFixedNode() {
			const centricToD3X = d3.scaleLinear().domain([-1, 1]).range([0, width]);
			const centricToD3Y = d3.scaleLinear().domain([-1, 1]).range([0, height]);
			for (let id in options.fixedNodes) {
				const fixed = options.fixedNodes[id];
				ev.whenInDom('#'+id, ()=>{
					const n = d3.select('#' + id).node().__data__;
					d3.select('#' + id).classed("fixed", n.fixed = true);
					n.fx = centricToD3X(fixed.x);
					n.fy = centricToD3Y(fixed.y);
				});
			}
			updateCenterForce();
		}

		function agitationTemporaire(duration, amplitude) {
			simulation.alphaTarget(amplitude).restart();
			setTimeout(function () {
				simulation.alphaTarget(0);
			}, Math.round(duration * 1000));
		}

		on("config.selected change", updateSelection);
		function updateSelection() {
			d3.selectAll('.selected').classed("selected", function (n) {
				return n.selected = false;
			});
			if (options.selected) {
				d3.select('#' + options.selected).classed("selected", function (n) {
					return n.selected = true;
				});
			}
			//allLinksG.selectAll("line").attr("stroke", (l)=>l.id===options.selected?'red':'grey');
			//updateDetails();
		}

		function updateCenterForce() {
			if (document.querySelectorAll(".fixed").length) simulation.force("center", undefined);
			else simulation.force("center", d3.forceCenter(width / 2, height / 2));
		}

		function computeNodesDegree(graph) {
			for (var i = 0; i < graph.node.length; ++i) graph.node[i].degree = 0;
			for (var i = 0; i < graph.link.length; ++i) {
				++graph.link[i].source.degree;
				++graph.link[i].target.degree;
			}
		}

		function computeVoisins(graph) {
			for (let i = 0; i < graph.node.length; ++i) {
				graph.node[i].voisins = [];
			}
			for (let i = 0; i < graph.link.length; ++i) {
				graph.link[i].source.voisins.push(graph.link[i].target);
				graph.link[i].target.voisins.push(graph.link[i].source);
			}
		}

		function updateZoom() {
			var zoom = options.zoom;
			if (!zoom) return;
			zoomableContainer.attr("transform",
				"translate(" + centerRatioX2D3X(zoom.x) + "," + centerRatioY2D3Y(zoom.y) + ") scale(" + zoom.k + ")");
		}

		function d3X2CenterRatioX(d3x, k) {
			if (!k) k = options.zoom.k;
			return d3x / (width / 2) + k - 1;
		}//(2*d3x/width-1)*k; }
		function d3Y2CenterRatioY(d3y, k) {
			if (!k) k = options.zoom.k;
			return d3y / (height / 2) + k - 1;
		}

		function centerRatioX2D3X(x) {
			return (1 + x - options.zoom.k) * width / 2;
		}

		function centerRatioY2D3Y(y) {
			return (1 + y - options.zoom.k) * height / 2;
		}

		function round(number, decimals) {
			if (!decimals) decimals = 0;
			return Math.round(number * Math.pow(10, decimals)) / Math.pow(10, decimals);
		}

		// fixedNodeFactory
		function regularPolygon(x, y, radius, sides) {
			var crd = [];
			if (sides == 1) return [[x, y]];
			for (var i = 0; i < sides; i++) {
				crd.push({
					"x": (x + (Math.sin(2 * Math.PI * i / sides) * radius)),
					"y": (y - (Math.cos(2 * Math.PI * i / sides) * radius))
				});
			}
			return crd;
		}

		function points2Path(points, tension) {
			var drawer = d3.line()
				.x(function (d) {
					return d.x;
				})
				.y(function (d) {
					return d.y;
				})
			//.curve(d3.curveCardinalClosed.tension(tension));
			return drawer(points);
		}
	});
	function build_imageLoadErrorPlaceOverFunc(id) { //FIXME: doublon legendView. La ranger mieux (et la coder)
		return (e) => {
			//FIXME: if svg fail -> png, if png fail -> identicon
			//https://github.com/davidbau/seedrandom

			//https://www.khanacademy.org/computer-programming/random-face-generator/6612995667394560
			//http://svgavatars.com/
			//http://bl.ocks.org/enjalot/1282943
			//https://github.com/alexvandesande/blockies
			//https://github.com/dmester/jdenticon
			// default image
			d3.event.target["xlink:href"] = options.defaultPicto;
		}
	}

	function getNearestAncestorId(node) {
		const ancestors = json2dom.xpath('ancestor::*[@id]', node, XPathResult.ORDERED_NODE_ITERATOR_TYPE);
		const layerNode = ancestors[ancestors.length - 1];
		return layerNode.id;
	}

	function partialHideNotConcerned(e) {
		const id = e.id;
		document.querySelectorAll('.node,.link').forEach((n) => n.classList.add("nearlyHidden"));
		document.querySelectorAll('#' + id + ',.node[class*="voisin_' + id + '"],.link[class*="voisin_' + id + '"]').forEach(
			(n) => n.classList.remove("nearlyHidden")
		);
	}

	function unhideAll(e) {
		document.querySelectorAll('.node,.link').forEach((n) => n.classList.remove("nearlyHidden"));
	}

	return {};
});
