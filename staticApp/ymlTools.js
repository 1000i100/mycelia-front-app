define([
	'js-yaml',
	'./structManipulation',
	'./smartEvents'
], (jsyaml, struct, ev) => {
	const on = ev.on, send = ev.send;
	function init() {
		on('file.ready', convert);
	}

	init();

	async function loadMerge(sourcesFiles, callbackOrEvent) {
		const merged = (await multiLoad(sourcesFiles)).map(jsyaml.safeLoad).reduce(struct.merge,{});
		if(callbackOrEvent) ev.callbackOrEventSender(callbackOrEvent, merged);
		return merged;
	}

	async function multiLoad(sourcesFiles) {
		let filesContentPromises = [];
		for (let file of sourcesFiles) {
			filesContentPromises.push(load(file));
		}
		return await Promise.all(filesContentPromises);
	}

	function ajax(url, data = undefined) {
		return new Promise( (resolve, reject) => {
				let postData;
				const request = new XMLHttpRequest();
				request.onload = function () {
					this.status === 200 ? resolve(this.response) : reject(this.statusText);
				};
				request.onerror = function () {reject('XMLHttpRequest Error: ' + this.statusText);};
				if (!data) request.open('GET', url, true);
				else {
					request.open('POST', url, true);
					postData = json2post(data);
				}
				request.crossDomain = true;
				request.withCredentials = true;
				request.send(postData);
			});
	}
	function json2post(json){
		const post = new FormData();
		for(let key in json){
			post.append(key,JSON.stringify(json[key]));
		}
		return post;
	}

	async function load(filePath) {
		const fileContent = await ajax(filePath);
		send('file.ready', {'filename': filePath, 'fileContent': fileContent});
		return fileContent;
	}

	function convert(fileLoadedEvent) {
		let yml = jsyaml.safeLoad(fileLoadedEvent.fileContent);
		if (!yml || yml === 'undefined') yml = {};
		send('yml.ready', {
			'filename': fileLoadedEvent.filename,
			'yml': yml
		});
	}

	// http://webreflection.blogspot.fr/2011/08/html5-how-to-create-downloads-on-fly.html
	function exportAsFile(fileName, data) {
		const a = document.createElement('a');
		a.setAttribute('download', fileName + '.yml');
		const inertData = struct.clone(data);
		a.setAttribute('href', 'data:text/yaml;charset=utf-8,' + encodeURIComponent(jsyaml.safeDump(inertData)));
		ev.clickOn(a);
	}

	return {
		init,
		loadMerge,
		load,
		multiLoad,
		convert,
		exportAsFile
	}
});
