define(['./ymlTools', './smartEvents'], (app, ev) => {
	describe('ymlTools', () => {
		beforeEach(()=>{
			ev.reset();
		});
		afterEach(ev.reset);
		it('convert ymlText to jsObject', () => {
			const dummyCallback = new Spy();
			const inputData = {"filename": "osef", "fileContent": "plop:\n  id: 0\n  label: Publication"};
			const expected = {'filename': 'osef', 'yml': {'plop': {'id': 0, 'label': 'Publication'}}};
			ev.on('yml.ready', dummyCallback);
			app.convert(inputData);
			expect(dummyCallback).toHaveBeenCalledWith(expected);
		});
		it('loadFile', (done) => {
			ev.on('file.ready', done);
			app.load('/base/staticApp/ymlTools.js');
		});
		it('loadFile async/await way', async () => {
			expect(await app.load('/base/staticApp/ymlTools.js')).toMatch("load");
		});
		it('multiLoad', (done) => {
			ev.after('file.ready file.ready', done);
			app.multiLoad(['/base/staticApp/ymlTools.js', '/base/staticApp/ymlTools.test.js']);
		});
		it('multiLoad async/await way', async () => {
			const filesContent = await app.multiLoad(['/base/staticApp/ymlTools.js', '/base/staticApp/ymlTools.test.js']);
			expect(filesContent[0]).toMatch("load");
			expect(filesContent[1]).toMatch("describe");
		});
		it("loadMerge with good overwriting legacy way",(done)=>{
			app.loadMerge([
				'/base/staticApp/ymlTools.test.z1.yaml.js',
				'/base/staticApp/ymlTools.test.z2.yaml.js'
			],(result)=>{
				expect(result.first).toBe(true);
				expect(result.second).toBe("overwritten");
				done();
			});
		});
		it("loadMerge with good overwriting async/await way",async ()=>{
			const result = await app.loadMerge([
				'/base/staticApp/ymlTools.test.z1.yaml.js',
				'/base/staticApp/ymlTools.test.z2.yaml.js'
			]);
			expect(result.first).toBe(true);
			expect(result.second).toBe("overwritten");

		});
	});
});
